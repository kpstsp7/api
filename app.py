from flask import Flask, jsonify, abort, make_response, request
import os
import gnupg

NOT_FOUND = 'Not found'
BAD_REQUEST = 'Bad request'


app = Flask(__name__)

#decrypr function
def decrypt_mess_data(inmess, passph):
    gpg = gnupg.GPG()
    en_str=inmess
    de_data = gpg.decrypt(str(en_str), passphrase=passph, always_trust ='true')

    return de_data



@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': NOT_FOUND}), 404)

#if request not contains json
@app.errorhandler(400)
def bad_request(error):
    return make_response(jsonify({'error': BAD_REQUEST}), 400)



@app.route('/decryptMessage', methods=['POST'])
def messg_post():
    print request.mimetype
#   Debug point
    print request.get_json(force=True)
    if not request.json:
        abort(400)
    else:
        rmess = request.json.get('Message')
        rpassph = request.json.get('Passphrase')
        print rmess
        print rpassph
#DONE: add processing incorrect key
        de_result=decrypt_mess_data(rmess, rpassph)
        if len(str(de_result))>0:
            answer=str(de_result).rstrip()
        else:
            abort(400)


    return jsonify({'Message': answer}), 201



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
