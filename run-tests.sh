echo "Prepare vinrtualenv"
virtualenv tst
. tst/bin/activate
pip install -r requirements.txt

echo "Run unit tests:"
python app_tests.py -v

