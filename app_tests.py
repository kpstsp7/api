import os
import app
import unittest
import tempfile
import json


def loadMess():
    with open('cryptedmess_test.json') as data_file:
        data = json.load(data_file, strict=False)
    return data





class apiTestCase(unittest.TestCase):

    def setUp(self):
        self.app = app.app.test_client()
#Check through correct passphrase
    def test_decryptMessage(self):
        print "--TEST : Check complete payload process--"
        print "\n"
        message = loadMess()
        response = self.app.post(
                '/decryptMessage',
        data=json.dumps(message),
        content_type='application/json')
        self.assertEqual(response.status_code, 201)
        print "> Response code status : " + str(response.status_code)
        print "\n"
        print "--TEST : complete --"
        print "\n\n"

#Check access to incorrect point
    def test_incorrect_path(self):
        print "--TEST: Check incorrect access point --"
        print "\n"
        message = loadMess()
        response = self.app.post(
                '/somewhere',
        data=json.dumps(message),
        content_type='application/json')
        self.assertEqual(response.status_code, 404)
        print "> Response code status : " + str(response.status_code)
        print "\n"
        print "--TEST : complete --"
        print "\n\n"
#Check bad request
    def test_incorrect_pass_mess(self):
        print "--TEST: Check incorrect content payload"
        print "\n"
        message = {"pass":"incorrectpassphrase"}
        response = self.app.post(
                '/decryptMessage',
        data=json.dumps(message),
        content_type='application/json')
        self.assertEqual(response.status_code, 400)
        print "> Response code status : " + str(response.status_code)
        print "\n"
        print "--TEST : complete --"
        print "\n\n"


if __name__ == '__main__':
    unittest.main()
