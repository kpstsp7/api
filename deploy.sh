yum install -y httpd24 mod24_wsgi-python27.x86_64 gcc
chkconfig httpd on
mkdir /var/www/flask
cd /var/www/flask/
cp -R /usr/local/src/* . && rm -rf *.sh
virtualenv env
. env/bin/activate
pip install -r requirements.txt
cp /usr/local/src/vhost.conf /etc/httpd/conf.d/vhost.conf
chown -R apache /var/www/ && service httpd start

#check web service status
echo "Check web service status: "
curl -vX POST --header "Content-Type: application/json" -d @cryptedmess.json http://localhost/decryptMessage


